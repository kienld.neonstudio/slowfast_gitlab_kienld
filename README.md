I use the repo "https://github.com/facebookresearch/SlowFast" to handle the "Action recognization task". This model can recognize 80 type of actions

For more details about architecture, please see original paper "https://arxiv.org/pdf/1812.03982.pdf" or see my presentation at Slowfast.pptx

**Install with docker: (Recommendation)

docker pull kienld304/docker_slowfast

docker images #check images

docker run -d -p 8000:8000 kienld304/docker_slowfast sleep infinity

docker ps #check container

docker exec -it ${ID or name} /bin/bash

python3  run_net.py --cfg /data/SlowFast-pj/SlowFast/demo/AVA/SLOWFAST_32x2_R101_50_50.yaml


**Install with conda env :

1. create env: python==3.7.2 and activate it
2. git clone https://github.com/facebookresearch/SlowFast
3. pip install numpy simplejson opencv-python psutil tensorboard moviepy scipy pandas scikit-learn
pip3 install torch==1.10.0+cu113 torchvision==0.11.1+cu113 torchaudio==0.10.0+cu113 -f
/ https://download.pytorch.org/whl/cu113/torch_stable.html
pip install setuptools==59.5.0
4. pip install 'git+https://github.com/facebookresearch/fvcore'
5. conda install av -c conda-forge
6. pip install -U iopath 
7. install opencv-python
8. install detectron2 by:
python -m pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu113/torch1.10/index.html
9. git clone https://github.com/facebookresearch/pytorchvideo then cd pytorchvideo
    pip install -e .    
- save ava.json file /data/SlowFast-pj/SlowFast/demo/AVA
- change config file SLOWFAST_32x2_R101_50_50.yaml in the directory: ./SlowFast/demo/AVA
- create Vinput, Voutput folder
10. copy:

import os
import sys
PARENT_PATH = os.path.dirname(os.path.abspath(__file__))
if  PARENT_PATH not in sys.path:
    sys.path.append(PARENT_PATH)
GRANDPARENT_PATH = os.path.dirname(PARENT_PATH)
if  GRANDPARENT_PATH not in sys.path:
    sys.path.append(GRANDPARENT_PATH)
GRANDGRANDPARENT_PATH = os.path.dirname(GRANDPARENT_PATH)
if  GRANDGRANDPARENT_PATH not in sys.path:
    sys.path.append(GRANDGRANDPARENT_PATH)

into file run_net.py

11. - download model https://dl.fbaipublicfiles.com/pyslowfast/model_zoo/ava/SLOWFAST_32x2_R101_50_50.pkl
save to configs/AVA/c2/
- cd slowfast python tools/run_net.py --cfg demo/AVA/SLOWFAST_32x2_R101_50_50.yaml

*** You can change the input video by Uploading your video to Folder Vinput and modify the path at DEMO.INPUT_VIDEO in demo/AVA/SLOWFAST_32x2_R101_50_50.yaml




***TRAING

The DATASET AVA2.2 contains contains 430 videos split into 235 for training, 64 for validation, and 131 for test. Almost videos are from Youtube.Then they cut orgirin video to new from 15 min to 30 min.
Each new video has 15 minutes.

They split 1 second to 30 frames. Choose the middle frame to label it.
This is one example of the format of AVA 2.2 (you can see the details in "https://research.google.com/ava/download.html")

-5KQ66BBWC4,0902,0.077,0.151,0.283,0.811,80,1

5KQ66BBWC4 : name of video 15 min
0902 : frame at 0902 ~ starting at 15 min (30 frame * 60 sec = 900 )
0.077,0.151,0.283,0.811 : coordinates of bouding box
80 : action id (details : anademo/annotations/ava_action_list_v2.2_for_activitynet_2019.pbtxt)
1: ID person


Please prepare the format folder the same my folder - anademo. Then open demo/AVA/SLOWFAST_32x2_R101_50_50.yaml and change mode Train to True. if you have more than 1 gpu, change the "NUM_GPUS"

Run python tools/run_net.py --cfg demo/AVA/SLOWFAST_32x2_R101_50_50.yaml

* reduce the BATCH_SIZE at TRAIN if meet error out off cuda memory. For me, with Batch size = 10 load 100 % card rtx 3090 24G,


